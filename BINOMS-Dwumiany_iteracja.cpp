/*
BINOMS - Dwumiany
tablica zwraca wartość współczynnika w zadanym wierszu i kolumnie (odpowiednio n i k)
*/
#include <iostream>

using namespace std;

int main()
{
  int n; // wiersz
  int k; // kolumna
  short t; // testy
  cin >> t;
  while(t--)
  {
    cin >> n >> k;

    unsigned int **pascal;
    pascal = new unsigned int *[n];

    for (int j=0; j<=n; j++)
    {
      pascal[j] = new unsigned int [j+1];
      pascal[j][0] = 1;
      pascal[j][j] = 1;
      for (int i=0; i<j-1; i++)
        pascal[j][i+1] =
          pascal[j-1][i] + pascal[j-1][i+1];
    }
    cout << pascal[n][k] << endl;

    for (int i = 0; i <= n; ++i)
      delete [] pascal[i];
    delete [] pascal;
  }
  return 0;
}
