/*
PP0601A2 - Test 3
*/

#include <iostream>

using namespace std;

int main()
{
  int n1, n2, l=0, temp;
  cin >> temp;
  cout << temp << endl;
  while(l<3){
    cin >> n2;
    if ((n2 == 42) && (temp != 42))
      l++;
    cout << n2 << endl;
    temp = n2;
  }
  return 0;
}
