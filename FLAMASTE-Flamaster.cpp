/*
FLAMASTE - Flamaster
zastępuję takie same litery ich liczbą (powyżej dwóch)
wyświetla pierwszą literę, a następnie ich łączną ilość
*/

#include <iostream>

using namespace std;

int main()
{
  int n;
  cin >> n;

  int l;
  string st;
  for (int j=0; j<n; j++)
  {
    cin >> st;
    for (int i=0; i<st.length(); i++)
    {
      l=0;
      while (st[i] == st[i+1])
      {
        l++;
        i++;
      }
      if (l >= 2) cout << st[i] << l+1;
      else if (l == 1) cout << st[i-1] << st[i];
        else cout << st[i];
      }
      cout << endl;
    }
  return 0;
}
