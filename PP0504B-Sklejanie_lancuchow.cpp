/*
PP0504B - StringMerge
sklejanie łańcuchów
*/

#include <iostream>
#include <cstring>

using namespace std;

#define T_SIZE 1001

char* string_merge(char *, char *, char *);

int main()
{
  int t,n;
  char S1[T_SIZE], S2[T_SIZE];
  char *O = new char[T_SIZE];

  cin >> t; // liczba testów
  cin.getline(S1,T_SIZE);
  while(t)
  {
    cin.getline(S1,T_SIZE,' '); // oczekuje na znak 'space' (0x20)
    cin.getline(S2,T_SIZE); // oczekuje na znak 'enter' ('\n')
    cout << string_merge(S1, S2, O) << endl;
    t--;
  }
 delete [] O;
 return 0;
}

char* string_merge(char *pS1, char *pS2, char *pO)
{
 short i=0, j=0;
 while((pS1[i] != '\0') && (pS2[i] != '\0')) // getline kończy znakiem '\0'
 {
     pO[j] = pS1[i];
     pO[j+1] = pS2[i]; // sklej naprzemiennie z każdego łańcucha
     j = j + 2;
     i++;
 }
 pO[j] = '\0'; // zamknięcie ciągu (poprzedni mógł być dłuższy)
 return pO;
}
