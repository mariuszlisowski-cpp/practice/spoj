/*
JGGHACK - Łamacz gg
*/

#include <iostream>
#include <cctype>

using std::cin;
using std::cout;
using std::endl;
using std::string;

int main()
{
  string s;
  typedef string::const_iterator iter;
  while (cin >> s) {
    for (iter it = s.begin(); it != s.end(); it += 2) {
      /* zamiana zakodowanych par liter na ASCII */
      int n = int(*it) - 65; // litera pierwsza (A=0...P=15 co 1)
      int m = (int(*(it+1)) - 65) * 16; // druga (A=0...P=240 co 16)
      cout << char(n + m);
    }
    cout << endl;
  }
  return 0;
}
