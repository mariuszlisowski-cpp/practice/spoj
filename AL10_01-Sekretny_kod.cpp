/*
AL_10_01 - Sekretny kod
*/

#include <iostream>
#include <algorithm>
#include <cmath>

using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::count;

int main()
{
  int t, n, counter;
  unsigned long long result;
  cin >> t;
  string s;
  while (t--) {
    cin >> n;
    cin >> s;
    counter = count(s.begin(), s.end(), '?');
    result = 1;
    if (counter == 0) result = 1;
    else
      if (counter == 1 && s.length() == 1) result = 10;
      else
       if (s[0] == '?') {
         result *= 9;
         result *= pow(10, counter - 1);
       }
       else result *= pow(10, counter);
    cout << result << endl;
  }
  return 0;
}
