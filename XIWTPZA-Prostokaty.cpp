/*
XIWTPZA - Prostokąty
algorytm wg W.Carver (z książki: John E. Wetzel "Rectangles in rectangles")
*/

#include <iostream>
#include <cmath>

using namespace std;

int main()
{
  short t;
  cin >> t;
  while(t)
  {
    double a, b, p, q; // a, b - duży; p, q - mały; a, p - dłuższe boki
    cin >> a >> b >> p >> q;
    double tmp;
    if (a < b) // odpowiednie przyporządkowanie dłuższego boku
    {
      tmp = a;
      a = b;
      b = tmp;
    }
    if (p < q) // odpowiednie przyporządkowanie dłuższego boku
    {
      tmp = p;
      p = q;
      q = tmp;
    }
    /* 1 warunek: (p < a) && (q < b) - boki małego mniejsze od boków dużego
       2 warunek: (p > a) - bok małego dłuższy od boku dużego i ... */
    if (((p < a) && (q < b)) || (((p > a) && ((b > (((2*p*q*a) + (p*p - q*q) * sqrt( p*p + q*q - a*a ))/(p*p + q*q))))))) cout << "TAK" << endl;
    else  cout << "NIE" << endl;
  t--;
  }
  return 0;
}
