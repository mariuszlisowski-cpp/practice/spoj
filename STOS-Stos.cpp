/*
STOS - Stos
*/

#include <iostream>
#include <vector>

using namespace std;

void dodaj(vector<int> &);
void zdejmij(vector<int> &);

int main()
{
  char znak;
  vector<int> stos;

  while (cin >> znak) {
    switch (znak) {
      case '+': dodaj(stos);
                break;
      case '-': zdejmij(stos);
                break;
      default: cout << ":(" << endl;
                break;
    }
  }

  return 0;
}

void dodaj(vector<int> &aStos)
{
  int l;
  cin >> l;
  if (aStos.size() < 10) {
    aStos.push_back(l);
    cout << ":)" << endl;
  }
  else
    cout << ":(" << endl;
}

void zdejmij(vector<int> &aStos)
{
  if (aStos.size() > 0 ) {
    cout << aStos[aStos.size()-1] << endl;
    aStos.pop_back();
  }
  else
    cout << ":(" << endl;
}
