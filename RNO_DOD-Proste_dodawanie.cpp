/*
RNO_DOD - Proste dodawanie
dodaje wszystkie liczby całkowite podane na wejściu
*/

#include <iostream>
#include <string>
using namespace std;

int main()
{
  short int t;
  cin >> t;
  for (int i=0; i<t; i++)
  {
    short int n;
    cin >> n;
    int l[n];
    int sum = 0;
    for (int j=0; j<n; j++)
    {
      cin >> l[j];
      cin.ignore();
      sum += l[j];
    }
    cout << sum << "\n";
  }

  return 0;
}
