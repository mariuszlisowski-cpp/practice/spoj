/*
PP0504D - Reprezentacja liczb typu float
reprezentacja szesnastkowa liczby w pamięci
*/

#define ANSI
#include <iostream>
#include <cstring>

using namespace std;

void printfloat(float);

int main()
{
  int t;
  float x;
  cin >> t;
  while(t)
  {
    cin >> x;
    printfloat(x);
    t--;
  }

return 0;
}

void printfloat(float aX)
{
  unsigned char* c; // wskaźnik do tablicy elementów jednobajtowych
  c = (unsigned char*)&aX; // adres zmiennej typu 'float' zrzutowany na wskaśnik do tablicy o elementach jednobajtowych

  short s = sizeof(aX); // wielkość zmiennej typu 'float'
  while (s) // bo adresowanie LE, tj. 'Little Endian' (od prawej do lewej)
  {
    cout << hex << (unsigned int)c[s-1] << " "; // rzutowanie na 'int' dla 'hex' (indeks o 1 mniejszy, bo to tablica)
    s--;
  }
  cout << endl;
}
