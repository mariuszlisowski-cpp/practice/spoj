/*
ROWNANIE - Równanie kwadratowe
*/
#include <iostream>

int main()
{
  double a, b, c, delta;

  while(std::cin >> a >> b >> c)
  {
    delta = b*b - 4*a*c;

    if (delta > 0)
      std::cout << 2;
    else
      if (delta == 0)
        std::cout << 1;
      else
        std::cout << 0;
    std::cout << std::endl;
  }
  return 0;
}
