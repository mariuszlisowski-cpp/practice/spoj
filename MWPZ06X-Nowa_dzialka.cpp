/*
MWPZ06X - Nowa działka
*/

#include <iostream>

using namespace std;

int main()
{
  int t, k;
  cin >> t;
  while (t--) {
    cin >> k;
    cout << k * k << endl;
  }

  return 0;
}
