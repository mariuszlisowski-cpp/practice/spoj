// number of lines in a file
// #fstream # ifstream

#include <iostream>

using namespace std;

int main() {
   int count = 0;
   string s;
   while (getline(cin, s)) {
      count++;
   }
   cout << count << endl;

   return 0;
}
