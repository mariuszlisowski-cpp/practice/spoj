/*
JSZYCER - Szyfr Cezara
Przesuwa alfabet o 3 miejsca w prawo
*/

#include <iostream>
#include <cctype>

using namespace std;

int main() {
  string s;
  while (getline(cin, s)) {
    for (int i=0; i < s.length(); i++) {
        switch (s[i]) {
          case 'X': s[i] = 'A'; break; // warunek brzegowy
          case 'Y': s[i] = 'B'; break; // j.w.
          case 'Z': s[i] = 'C'; break; // j.w.
          default:
            if (!isspace(s[i])) // jeśli znak NIE jest spacją
              s[i] = int(s[i]) + 3; // przesunięcie wartości ASCII
            break;
        }
    }
    cout << s << endl;
  }
  return 0;
}
