/*
SYS - Systemy pozycyjne
*/

#include <iostream>
#include <algorithm>
#include <string>

using namespace std;

string convert(int, int);

int main()
{
  int t, p, n;

  cin >> t;
  while (t) {
    cin >> n;
    cout << convert(n, 16) << " " << convert(n, 11) << endl;
    t--;
  }
  return 0;
}

string convert(int aN, int aP) {
  int m;
  char mod;
  string s;
  while (aN > 0) {
    m = aN % aP;
    aN = aN / aP;
    if (m > 9) {
      switch (m) {
        case 10: mod = 'A'; break;
        case 11: mod = 'B'; break;
        case 12: mod = 'C'; break;
        case 13: mod = 'D'; break;
        case 14: mod = 'E'; break;
        case 15: mod = 'F'; break;
      }
      s += mod;
    }
    else
      s += to_string(m);
  }
  reverse(s.begin(), s.end());
  return s;
}
