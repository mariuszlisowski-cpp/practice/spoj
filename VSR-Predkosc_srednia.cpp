/*
VSR - Prędkość średnia
*/

#include <iostream>

using namespace std;

int main()
{
  int t;
  cin >> t;

  int v1, v2;
  for (int i=0; i<t; i++)
  {
    cin >> v1 >> v2;
    cout << 2 * (v1 * v2) / (v1 + v2) << "\n";
  }
  return 0;
}
