/*
GLUTTON - Obżartuchy
*/

#include <iostream>
#include <cmath>
#include <iomanip>

using namespace std;

int main()
{
  short n;
  cin >> n; // liczba testów

  int d = 60*60*24; // długość doby w sekundach (60s * 60m * 24h)
  while(n)
  {
    int o, c; // obżartuchy, liczba ciastek w pudełku
    cin >> o >> c;

    int t[o];
    for (int i=0; i<o; i++) // wczytywanie czasów zjedzenia
      cin >> t[i];

    int s = 0;
    for (int i=0; i<o; i++) // obliczenia
      s += floor(d / t[i]); //m[i]; // wszystkie zjedzone ciastka
    /* floor(d / t[i]); max ciastek dla każdego jedzącego */

    cout << ceil(1.0 * s/c) << endl; // liczba pudełek z ciastkami

    n--;
  }
  return 0;
}
