/*
EUCGAME - Gra Euklidesa
*/
#include <iostream>

using namespace std;

int main()
{
  short t;
  cin >> t;

  while(t)
  {
    int a, b;
    cin >> a >> b;

    while (a != b)
    {
      if (a < b) b = b-a;
      else a = a-b;
    }
    cout << a+b << endl;
    t--;
  }
  return 0;
}
