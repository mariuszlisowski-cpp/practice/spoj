/*
PA05_POT - Czy umiesz potęgować
wyznacza ostatnią cyfrę potęgi
wykorzystuje prawidłowości zamiast obliczać potęgi (szybki algorytm)
*/

#include <iostream>
#include <cmath>

using namespace std;

int main()
{
  int n;
  cin >> n;

  long a, b;

  for (int i=0; i<n; i++)
  {
    cin >> a >> b;
    long long w;
    int s = (int)fmod(a,10); // 'floating modulo' ostatnia cyfra podstawy (rzutowana)
    switch(s)
    {
      case 0: w = 0; break;
      case 1: w = 1; break;
      case 2:
      {
        if ((b % 4) == 1) w = 2;
        if ((b % 4) == 2) w = 4;
        if ((b % 4) == 3) w = 8;
        if ((b % 4) == 0) w = 6;
        break;
      }
      case 3:
      {
        if ((b % 4) == 1) w = 3;
        if ((b % 4) == 2) w = 9;
        if ((b % 4) == 3) w = 7;
        if ((b % 4) == 0) w = 1;
        break;
      }
      case 4:
      {
        if ((b % 2) == 1) w = 4;
        if ((b % 2) == 0) w = 6;
        break;
      }
      case 5: w = 5; break;
      case 6: w = 6; break;
      case 7:
      {
        if ((b % 4) == 1) w = 7;
        if ((b % 4) == 2) w = 9;
        if ((b % 4) == 3) w = 3;
        if ((b % 4) == 0) w = 1;
        break;
      }
      case 8:
      {
        if ((b % 4) == 1) w = 8;
        if ((b % 4) == 2) w = 4;
        if ((b % 4) == 3) w = 2;
        if ((b % 4) == 0) w = 6;
        break;
      }
      case 9:
      {
        if ((b % 2) == 1) w = 9;
        if ((b % 2) == 0) w = 1;
        break;
      }
    }
    cout << w;
    cout << endl;
  }
  return 0;
}
