/*
NIEKOLEJ - Niekolejne
Wypisuje ciąg w dowolnej kolejności, przy czym żadne dwie sąsiednie liczby nie mogą różnić się o 1
*/

#include <iostream>

using namespace std;

int main()
{
  unsigned long int n, j, mid;
  cin >> n; // ciąg 0....n
  unsigned long int tab[n+1];

  for (int i = 0; i < n + 1; i++)
    tab[i] = i; // zapełnianie tablicy ciągiem liczb

  mid = n / 2; // środek ciągu (nawet o nieparzystej długości)

  if (n > 2) { // przy 1 i 2 rozwiązanie nie jest możliwe
    if ((n+1) % 2 == 0) { // z zerem ciąg parzysty przy nieparzystym 'n'
      cout << tab[mid+1] << " "; // ze środka+1 na początek
      j = n;
      for (int i = 0; i < mid; i++) {
        cout << tab[i] << " " << tab[j] << " "; // od skrajnych do środkowych
        j--;
      }
      cout << tab[mid] << " "; // ze środka na koniec
    }
    else {
      cout << tab[mid] << " "; // środkowy na początek
      j = n;
      for (int i = 0; i < mid; i++) {
        cout << tab[i] << " " << tab[j] << " "; // od skrajnych do środkowych
        j--;
      }
    }
  }
  else {
    if (n == 0)
      cout << 0; // warunek dla zera
    else
      cout << "NIE"; // 1 i 2 niemożliwe do rozwiązania
  }

  cout << endl;
  return 0;
}
