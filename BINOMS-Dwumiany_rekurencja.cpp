/*
BINOMS - Dwumiany
~ tablica zwraca wartość współczynnika w zadanym wierszu i kolumnie (odpowiednio n i k)
*/

#include <iostream>

using namespace std;

long long pascal(int, int);

int main()
{
  int n; // wiersz
  int k; // kolumna
  short t; // testy
  cin >> t;
  while(t)
  {
    cin >> n >> k;
    cout << pascal(n, k) << endl;
    --t;
  }
  return 0;
}

long long pascal(int n, int k)
{
  if ((k == 0) || (k == n))
     return 1;
  else
     return pascal(n-1, k-1) + pascal(n-1, k);
}
