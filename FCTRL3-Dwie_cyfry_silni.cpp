/*
FCTRL3 - Dwie cyfry silni
wyświetla 2 ostatnie cyfry silni
wykorzystuje prawidłowości (np. powyżej 10! są to zera)
*/

#include <iostream>

using namespace std;

int main()
{
  int t;
  cin >> t;

  int n;
  int je, dz;
  long long silnia;
  for (int i=0; i<t; i++)
  {
    cin >> n;
    silnia = 1;
    if ((n > 1) && (n<10)) // obliczanie 2! do 9!
    {
      for (int j=1; j<=n; j++) silnia *= j;
      je = silnia % 10;
      dz = (silnia/10) % 10;
      cout << dz << " " << je << endl;
    }
    else if ((n == 0) || (n == 1))
      cout << 0 << " " << 1 << endl; // 0! 1!
      else cout << 0 << " " << 0 << endl; // powyżej 10!
  }
  return 0;
}
