/*
JSPACE - Spacje
Likwiduje spacje w zdaniach i rozpoczyna wyrazy wielką literą.
*/

#include <iostream>

using namespace std;

int main()
{
  int poz;
  string s;

  while (getline(cin, s)) {
    while (s.find(" ") != string::npos) {
      poz = s.find(" ");
      if (s[poz+1] != ' ') // jeśli więcej spacji...
        if (int(s[poz+1]) > 96) // zamieniaj tylko małe litery
          s[poz+1] = char(s[poz+1]-32);
      s.erase(poz, 1); // kasuj spację
    }
    cout << s << endl;
  }

  return 0;
}
