/*
AL_26_01 - DOMINO - znajdź kamień
input:
2|5 4|5 0|1 3|4 3|6 3|1 2|6 0|0 3|3 5|3 6|6 1|1 3|0 2|2 6|4 4|2 0|4 2|3 0|2 1|6 2|1 5|0 0|6 1|4 6|5 4|4 5|1
output:
5|5
*/

#include <iostream>
#include <algorithm>
using namespace std;

int main() {
    const int size = 7;
    int domino[size][size];

    int tests;
    cin >> tests;
    while (tests--) {
    // zerowanie tablicy jako znacznik
    for (int i = 0; i < size; i++)
            for (int j = 0; j < size; j++)
                domino[i][j] = 0;
    // mapa zapełnienia matrycy
    int a, b;
        char c;
        for (int i = 0; i < 27; i++) {
            cin >> a >> c >> b;
            domino[a][b] = 1;
            domino[b][a] = 1;
        }
    // wyszukiwanie brakującej kości
        for (int i = 0, end = 0; i < 8; i++) {
            for (int j = 0; j < size; j++) {
        if (domino[i][j] == 0) {
                    cout << i << "|" << j << endl;
                    end = 1;
                    break;
                }
            }
            if (end) break;
        }
    }
  return 0;
}
