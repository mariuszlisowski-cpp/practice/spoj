/*
KC003 - Nierówność trójkąta
W trójkącie suma długości dwóch dowolnych boków jest większa od długości trzeciego boku
*/

#include <iostream>

using std::cin;
using std::cout;
using std::endl;

int main()
{
  float x, y, z;
  while (cin >> x >> y >> z) {
    if ((x + y > z) && (x + z > y) && (y + z > x))
      cout << 1 << endl; // trójkąt taki instnieje
    else
      cout << 0 << endl;
  }
  return 0;
}
