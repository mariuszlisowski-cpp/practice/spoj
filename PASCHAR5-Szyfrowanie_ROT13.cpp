/*
PASCHAR5 - Szyfrowanie ROT13
*/

#include <iostream>
#include <cctype>

using std::cin;
using std::cout;
using std::endl;
using std::string;

char rot13(char);
char rot5(char);

int main()
{
  string s, ss;
  while (getline(cin, s)) {


    for (string::const_iterator it = s.begin(); it != s.end(); it++) {
      if (isalpha(*it))
        cout << rot13(*it);
      if (isdigit(*it))
        cout << rot5(*it);
      if (!isalpha(*it) && !isdigit(*it))
        cout << *it;
    }
    cout << endl;
  }
  return 0;
}

char rot13(char c) {
  return c-1 / (~(~c|32) / 13*2 - 11) * 13;
}

char rot5(char c) {
  if ( int(c)-48 >= 0 && int(c)-48 <= 4)
    return char(int(c)+5);
  else
    return char(int(c)-5);
}
