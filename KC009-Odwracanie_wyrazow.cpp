/*
KC009 - Odwracanie wyrazów
*/

#include <iostream>

using std::cin;
using std::cout;
using std::endl;
using std::string;

int main()
{
  int l;
  string s;
  while (cin >> s) {
    l = s.length();
    for (int i = 0; i < l; i++)
      cout << s[l-(i+1)];
    cout << endl;
  }
  return 0;
}
