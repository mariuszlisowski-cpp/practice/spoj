/*
BFN1 - Zabawne dodawanie Piotrusia
liczba palindrom (tzn. czy czyta się ją tak samo od lewej do prawej strony, jak od prawej do lewej)
*/

#include <iostream>
using namespace std;

int reverseNumber(int);

int main()
{

  int t;
  cin >> t;
  int arr[t];

  for (int i=0; i<t; i++)
    cin >> arr[i];

  int x=0;
  for (int i=0; i<t; i++)
  {
    while (arr[i] != reverseNumber(arr[i]))
    {
      arr[i] += reverseNumber(arr[i]);
      x++;
    }
    cout << arr[i] << " " << x << endl;
    x=0;
  }
  return 0;
}

// funkcja odwracająca numer
int reverseNumber(int aNumber)
{
  int reverse=0, rest;
  while(aNumber!=0)
  {
     rest = aNumber % 10;
     reverse = reverse * 10 + rest;
     aNumber /= 10;
  }
  return reverse;
}
