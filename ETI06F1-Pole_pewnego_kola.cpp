/*
ETI06F1 - Pole pewnego koła
obliczamy pole przekroju koła w pewnej odległoći od środka kuli
*/

#include <iostream>
#include <cmath>
#include <iomanip>

using namespace std;

int main()
{
  double r, d; // promień kuli, odległość od środków kul
  cin >> r >> d;

  double x = sqrt(pow(r, 2) - pow(d/2, 2)); // promień przekroju koła wg twierdzenie Pitagorasa (a^2 + b^2 = c^2)

  double p = M_PI * pow(x, 2); // pole przekroju koła

  cout << setprecision(2) << fixed << p << endl;
  return 0;
}
