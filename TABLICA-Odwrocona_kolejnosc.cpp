/*
TABLICA - Tablica
wypisuje wczytany ciąg liczb w odwróconej kolejności
*/

#include <iostream>
#include <vector>

using namespace std;

int main()
{
  int n;
  vector <int> vec;
  while (cin >> n)
    vec.push_back(n);

  for (short i=vec.size()-1; i>=0; i--)
    cout << vec[i];

  cout << endl;
  return 0;
}
