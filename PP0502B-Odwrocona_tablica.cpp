/*
PP0502B - Tablice
odwracanie kolejność elementów w tablicy
*/

#include <iostream>

using namespace std;

int main()
{
  short t;
  cin >> t; // ilość testów

  for (short i=0; i<t; i++)
  {
    short n;
    cin >>n; // ilość elementów do odwrócenia

    short d[n];
    for (short j=0; j<n; j++)
      cin >> d[j];
    for (short k=n-1; k>=0; k--)
      cout << d[k] << " ";
    cout << endl;
  }
  return 0;
}
